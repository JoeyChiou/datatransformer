﻿using MySql.Data.MySqlClient;
using MySql.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataTransFormer
{
    public partial class Form1 : Form
    {
        // 資料庫位址
        static string dbHost = "54.199.154.115";
        // 資料庫使用者帳號
        static string dbUser = "HongKong";
        // 資料庫使用者密碼
        static string dbPass = "goodtrader";
        // 資料庫名稱
        static string dbName = "HongKong";
        // MySQL Connection String
        static string connStr = "server=" + dbHost + ";uid=" + dbUser + ";pwd=" + dbPass + ";database=" + dbName;

        string rootpath;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                rootpath = folderBrowserDialog1.SelectedPath;
                toolStripProgressBar1.Maximum = Directory.GetFiles(rootpath, "*.csv", SearchOption.AllDirectories).Length;

                if (!backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.RunWorkerAsync();
                    toolStripProgressBar1.Value = 0;
                }
            }

            //FileStream fs;
            //StreamReader sr;
            //String StockID;

            //if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            //{
            //    string selectedPath = folderBrowserDialog1.SelectedPath;
            //    string[] files = Directory.GetFiles(selectedPath);

            //    toolStripProgressBar1.Maximum = files.Length;

            //    for (int i = 0; i < files.Length; i++)
            //    {
            //        try
            //        {
            //            fs = new FileStream(files[i], FileMode.Open, FileAccess.Read);
            //            sr = new StreamReader(fs);
            //            StockID = files[i].Split('\\').Last().Split('.').First();

            //            OleDbConnection connection = new OleDbConnection();
            //            connection.ConnectionString = ConfigurationManager.ConnectionStrings["HongKongDataConnectionString"].ToString();

            //            if (connection.State == ConnectionState.Open)
            //                connection.Close();

            //            connection.Open();
            //            OleDbCommand InsertCommand;

            //            string s = sr.ReadLine();
            //            while (!sr.EndOfStream)
            //            {
            //                s = sr.ReadLine();
            //                string insertString = "INSERT INTO StockData(StockID,sDate,sTime,Price,Volume) VALUES(@StockID,@sDate,@sTime,@Price,@Volume)";

            //                InsertCommand = new OleDbCommand(insertString, connection);
            //                InsertCommand.Parameters.AddWithValue("@StockID", StockID);
            //                InsertCommand.Parameters.AddWithValue("@sDate", s.Split(',')[0].Split(' ')[0].Replace('/', '-'));
            //                InsertCommand.Parameters.AddWithValue("@sTime", s.Split(',')[0].Split(' ')[1]);
            //                InsertCommand.Parameters.AddWithValue("@Price", s.Split(',')[1]);
            //                InsertCommand.Parameters.AddWithValue("@Volume", s.Split(',')[2]);
            //                InsertCommand.ExecuteNonQuery();
            //            }

            //            if (connection.State == ConnectionState.Open)
            //                connection.Close();

            //            toolStripProgressBar1.Value += 1;
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show("Error: " + ex.ToString() + " From File '" + files[i] + "'");
            //        }
            //    }

            //    MessageBox.Show("Import Successful!");
            //    toolStripProgressBar1.Value = 0;
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM StockData", conn);
            //execure the reader
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                //MessageBox.Show(reader.GetString(0));
            }

            reader.Close();
            conn.Close();
            /*if (!tbOutput.Text.Equals(string.Empty))
            {
                //SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["HongKongDataConnectionString"].ToString());
                OleDbConnection connection = new OleDbConnection();
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["HongKongDataConnectionString"].ToString();

                if (connection.State == ConnectionState.Open)
                    connection.Close();

                connection.Open();

                //SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM StockData WHERE StockID='" + tbOutput.Text.Trim() + "'", connection);
                OleDbDataAdapter adapter = new OleDbDataAdapter("SELECT * FROM StockData WHERE StockID='" + tbOutput.Text.Trim() + "'", connection);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                connection.Close();

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string fileName = saveFileDialog1.FileName;
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Date,Time,Price,Volume");

                    string writeString = "";
                    foreach (DataRow row in dt.Rows)
                    {
                        writeString = Convert.ToDateTime(row[1]).ToString("yyyy-MM-dd");
                        writeString += "," + row[2].ToString();
                        writeString += "," + row[3].ToString();
                        writeString += "," + row[4].ToString();
                        sb.AppendLine(writeString);
                    }

                    File.WriteAllText(fileName, sb.ToString());
                    MessageBox.Show("Export Successful!");
                }
            }
            else
            {
                MessageBox.Show("StockID can not be empty!");
            }*/
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["HongKongDataConnectionString"].ToString();

            if (connection.State == ConnectionState.Open)
                connection.Close();

            connection.Open();

            string[] files = Directory.GetFiles(rootpath, "*.csv", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    FileStream fs = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string StockID = files[i].Split('\\').Last().Split('.').First();

                    string s = sr.ReadLine();
                    while (!sr.EndOfStream)
                    {
                        s = sr.ReadLine();
                        string insertString = "INSERT INTO StockData(StockID,sDate,sTime,Price,Volume) VALUES(@StockID,@sDate,@sTime,@Price,@Volume)";

                        OleDbCommand InsertCommand = new OleDbCommand(insertString, connection);
                        InsertCommand.Parameters.AddWithValue("@StockID", StockID);
                        InsertCommand.Parameters.AddWithValue("@sDate", s.Split(',')[0].Split(' ')[0].Replace('/', '-'));
                        InsertCommand.Parameters.AddWithValue("@sTime", s.Split(',')[0].Split(' ')[1]);
                        InsertCommand.Parameters.AddWithValue("@Price", s.Split(',')[1]);
                        InsertCommand.Parameters.AddWithValue("@Volume", s.Split(',')[2]);
                        InsertCommand.ExecuteNonQuery();
                    }
                    backgroundWorker1.ReportProgress(i, files[i]);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.ToString() + " From File '" + files[i] + "'");
                }
            }

            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripProgressBar1.Value = e.ProgressPercentage;
            toolStripStatusLabel2.Text = e.ProgressPercentage + "/" + toolStripProgressBar1.Maximum;
            lblPSFile.Text = e.UserState.ToString();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Import Successful!");
            toolStripProgressBar1.Value = 0;
        }
    }
}
